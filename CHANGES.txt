v0.1.0, 22-08-2013 -- Initial release.
v0.1.1, 11-09-2013 -- Some improvements and bug fixing.
v0.1.2, 17-09-2013 -- GPSTime improvements to allow comparisons.
v0.1.3, 18-09-2013 -- GPSTime is now more robust to wrong parameter names and types. Also, it now accepts a time.struct_time as utc or localtime input.