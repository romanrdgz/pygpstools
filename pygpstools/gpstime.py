#!/usr/bin/env python

import calendar
from datetime import datetime, timedelta
from functools import total_ordering
from math import fmod
import time
import constants


@total_ordering
class GPSTime:
    '''
    Class GPSTime represents an instant of time for GPS usage.

    There are several ways of creating a GPSTime instance, depending
    on the parameters used:
    - 'gps_time': creates a GPSTime instance from a full GPS time amount.
    Eg: GPSTime(gps_time=1059319680)
    - 'tow' and 'wn': creates a GPSTime instance from a GPS time expressed
    with its week number and time of week. Eg: GPSTime(wn=1751, tow=314880)
    - 'utctime': (or 'utc') creates a GPSTime instance representing the
    equivalent GPS time to given UTC time.
    Eg: GPSTime(utc=datetime(2013, 7, 31, 15, 28, 0))
    - 'localtime': (or 'local') creates a GPSTime instance representing the
    equivalent GPS time to given local time, so time zone and Daylight Saving
    Time (DST) is considered. Eg: localtime=datetime(2013, 7, 31, 17, 28, 0))
    - No parameters: Creates a GPSTime instance for current gps time.
    '''
    def __init__(self, **kwargs):
        # By default, leap seconds will be considered
        self.ignore_leap_seconds = kwargs.get('ignore_leap_seconds', False)
        if ('gps_time' in kwargs) or ('gpstime' in kwargs) or \
           ('gps' in kwargs):
            # A total gps time is given as parameter
            self.gps_time = kwargs.get('gps_time')
            self.tow = int(fmod(self.gps_time, constants.seconds_one_week))
            self.wn = int(self.gps_time / constants.seconds_one_week)
        elif ('tow' in kwargs) and ('wn' in kwargs):
            # Gps time provided with tow and wn parameters
            self.tow = kwargs.get('tow')
            self.wn =  kwargs.get('wn')
            self.gps_time = self.wn * constants.seconds_one_week + self.tow
        elif ('utctime' in kwargs) or ('utc' in kwargs):
            # UTC time provided to be converted to gps time
            self.utc = kwargs.get('utc')
            # First check if UTC time is given as a time struct or a
            # datetime object
            if isinstance(self.utc, time.struct_time):
                self.utc = self._time_struct_to_datetime(self.utc)
            elif not isinstance(self.utc, datetime):
                raise Exception('GPSTime constructor only allows UTC time '
                    + 'given as time.struct_time or datetime instance')
            self._from_utc()
        elif ('localtime' in kwargs) or ('local' in kwargs):
            self.localtime = kwargs.get('localtime')
            # First check if local time is given as a time struct or a
            # datetime object
            if isinstance(self.localtime, time.struct_time):
                self.localtime = self._time_struct_to_datetime(self.localtime)
            elif not isinstance(self.localtime, datetime):
                raise Exception('GPSTime constructor only allows local time '
                    + 'given as time.struct_time or datetime instance')
            utc_offset = self._get_utc_offset()
            self.utc = self.localtime - timedelta(hours=utc_offset)
            self._from_utc()
        else:
            if len(kwargs) == 0:
                # No parameters provided, so will get current gps time
                self._get_gps_time_now()
            else:
                # Some unnamed or wrong parameter has been given
                raise Exception('Unnamed or wrong parameter given. Check '
                    + 'GPSTime.__doc__ for the right parameter names')

    def __repr__(self):
        return 'GPSTime %d (wn=%d, tow=%d)' % \
            (self.gps_time, self.wn, self.tow)

    def __add__(self, other):
        result = None
        if isinstance(other, GPSTime):
            result = GPSTime(gps_time=(self.gps_time + other.gps_time))
        elif isinstance(other, (int, long)):
            result = GPSTime(gps_time=(self.gps_time + other))
        return result

    def __radd__(self, other):
        result = None
        if isinstance(other, GPSTime):
            result = GPSTime(gps_time=(other.gps_time + self.gps_time))
        elif isinstance(other, (int, long)):
            result = GPSTime(gps_time=(other + self.gps_time))
        return result

    def __sub__(self, other):
        result = None
        if isinstance(other, GPSTime):
            result = GPSTime(gps_time=(self.gps_time - other.gps_time))
        elif isinstance(other, (int, long)):
            result = GPSTime(gps_time=(self.gps_time - other))
        return result

    def __rsub__(self, other):
        result = None
        if isinstance(other, GPSTime):
            result = GPSTime(gps_time=(other.gps_time - self.gps_time))
        elif isinstance(other, (int, long)):
            result = GPSTime(gps_time=(other - self.gps_time))
        return result

    def __eq__(self, other):
        result = False
        if isinstance(other, GPSTime):
            result = self.gps_time == other.gps_time
        elif isinstance(other, (int, long)):
            result = self.gps_time == other
        return result

    def __gt__(self, other):
        result = False
        if isinstance(other, GPSTime):
            result = self.gps_time > other.gps_time
        elif isinstance(other, (int, long)):
            result = self.gps_time > other
        return result

    def _get_gps_time_now(self):
        seconds_from_jan_1970 = int(time.time())
        seconds_from_jan_1980 = seconds_from_jan_1970 - \
         constants.seconds_from_1970_to_1980 - constants.five_days
         # + constants.one_hour
        self._from_1980(seconds_from_jan_1980)

    def _time_struct_to_datetime(self, time_struct):
        '''
        Converts the time struct into seconds since epoch, so then
        it can be converted to a datetime object
        '''
        seconds_since_epoch = time.mktime(time_struct)
        return datetime.fromtimestamp(seconds_since_epoch)

    def get_wn_rolled_over(self):
        '''
        Returns GPS week number rolled over. The GPS Week Number count began
        at approximately midnight on the evening of 05 January 1980 / morning
        of 06 January 1980. Since that time, the count has been incremented by
        1 each week, and broadcast as part of the GPS message. The GPS Week
        Number field is modulo 1024. This means that at the completion of week
        1023, the GPS week number rolled over to 0 on midnight GPS Time of the
        evening of 21 August 1999 / morning of 22 August 1999. Note that this
        corresponded to 23:59:47 UTC on 21 August 1999.
        '''
        if self.wn > constants.gps_week_rollover:
            return self.wn - constants.gps_week_rollover
        else:
            return self.wn

    def to_utc(self):
        '''
        Returns equivalent UTC time (seconds from 1st January 1970)
        '''
        # GPS time is the amount of seconds since Jan 1980. System time is set
        # in 1 Jan 1970 so:
        seconds_since_1Jan1970 = self.gps_time + \
         constants.seconds_from_1970_to_1980 + constants.five_days
        # Now create a datetime object and with MILLISECONDS since 1 Jan 1970:
        return datetime.utcfromtimestamp(seconds_since_1Jan1970)

    def to_localtime(self):
        '''
        Returns equivalent local time
        '''
        utc_offset = self._get_utc_offset()
        return self.to_utc() + timedelta(hours=utc_offset)

    def _from_utc(self):
        seconds_from_jan_1970 = calendar.timegm(self.utc.timetuple())
        seconds_from_jan_1980 = seconds_from_jan_1970 - \
         constants.seconds_from_1970_to_1980 - constants.five_days
        self._from_1980(seconds_from_jan_1980)

    def _from_1980(self, seconds_from_jan_1980):
        self.gps_time = seconds_from_jan_1980
        if self.ignore_leap_seconds:
            self.gps_time += 0  #constants.LEAP_SECONDS #TODO
        self.tow = int(fmod(self.gps_time, constants.seconds_one_week))
        self.wn = int(self.gps_time / constants.seconds_one_week)

    def _get_utc_offset(self):
        if time.daylight == 0:
            utc_offset = -time.timezone / constants.one_hour
        else: # Daylight Daving Time (DST)
            utc_offset = -time.altzone / constants.one_hour
        return utc_offset
