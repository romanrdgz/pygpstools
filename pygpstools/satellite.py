#!/usr/bin/env python

from math import sqrt, pow, fabs, sin, cos, atan2, acos, radians, degrees
import constants
import geodesy


class Satellite:
    '''
    Satellite class represents a GPS satellite.
    
    Provided almanac_data is a dict including the following keys:
    'ID', 'Eccentricity', 'Time of Applicability(s)', 'Orbital Inclination(rad)',
    'Rate of Right Ascen(r/s)', 'SQRT(A)  (m 1/2)', 'Right Ascen at Week(rad)',
    'Argument of Perigee(rad)', 'Mean Anom(rad)', 'Af0(s)', 'Af1(s/s)', 'week'
    '''
    def __init__(self, almanac_data):
        self.almanac_data = almanac_data
        self.prn = int(self.almanac_data['ID'])
        self.done_calculations = {}
        self.x, self.y, self.z = (None,) * 3
        self.vx, self.vy, self.vz = (None,) * 3


    def get_position(self, gps_time=None):
        '''
        This method returns the satellite coordinates (x, y, z) for a given
        GPS time. If no GPS time is given, last calculated coordinates
        will be returned.
        '''
        if gps_time != None:
            if self._get_position_from_almanacs(gps_time) is False:
                raise Exception('Position could not be computed for satellite with PRN ' +\
                 str(self.almanac_data['ID']))
        return self.x, self.y, self.z

    def get_velocity(self):
        '''
        This method returns the velocity components of the satellite object.
        These represent the satellite velocity for the gps time used last
        time get_position_from_almanacs method was called.
        If hasn't been calculated yet, will return None values.
        '''
        return self.vx, self.vy, self.vz

    def _get_position_from_almanacs(self, gps_time):
        '''
        This method calculates GPS position and velocity in WGS84 based on the almanacs
        for a given gps time. Will return True if position and velocity have been
        successfuly calculated, False otherwise.
        '''
        # Mean Motion:
        SemiAxis = float(self.almanac_data['SQRT(A)  (m 1/2)'])
        SemiAxis *= SemiAxis
        if SemiAxis < constants.min_orbit_radius:
            return False
        MeanMotion = sqrt(constants.mu_earth / pow(SemiAxis, 3))

        # Compute lapsed time since toe when required for end of week cross-over
        # The end of the week is not checked to leave the orbit propagate
        # more than one week
        gpsapplicableTime = (int(self.almanac_data['week']) + constants.gps_week_rollover) * \
         constants.seconds_one_week + float(self.almanac_data['Time of Applicability(s)'])
        Delta_T = gps_time.get_gps_time() - gpsapplicableTime

        # Mean Anomaly:
        MeanAnom = float(self.almanac_data['Mean Anom(rad)']) + MeanMotion * Delta_T

        # Solves Kepler for eccentric anomaly:
        e1 = float(self.almanac_data['Eccentricity'])
        e2 = e1*e1
        EccAnom = MeanAnom
        Converged = False
        Count = 0
        while (not Converged) and (Count < constants.kepler_ecc_an_eq_max_iter):
            Old_EccAnom = EccAnom
            EccAnom = MeanAnom + e1 * sin(EccAnom)
            Count+=1
            if fabs(EccAnom - Old_EccAnom) < constants.kepler_epsilon:
                Converged = True

        # Obtains true anomaly:
        sinEcc = sin(EccAnom)
        cosEcc = cos(EccAnom)
        sinvk = sqrt(1 - e2) * sinEcc / (1 - e1 * cosEcc)
        cosvk = (cosEcc - e1) / (1 - e1 * cosEcc)
        if (fabs(sinvk) < constants.sat_epsilon) and (fabs(cosvk) < constants.sat_epsilon):
            trueAnom = 0
        else:
            trueAnom = atan2(sinvk, cosvk)
        EccAnom = acos((e1 + cosvk) / (1 + e1 * cosvk))

        # Argument of Latitude:
        ArgLat = trueAnom + float(self.almanac_data['Argument of Perigee(rad)'])

        # Corrected radius, argument of latitude, inclination and ascending node:
        Radius = SemiAxis * (1 - e1 * cosEcc)
        ik = float(self.almanac_data['Orbital Inclination(rad)'])
        AscNode = float(self.almanac_data['Right Ascen at Week(rad)']) + \
         Delta_T * (float(self.almanac_data['Rate of Right Ascen(r/s)']) - constants.omega_earth) \
         - constants.omega_earth * float(self.almanac_data['Time of Applicability(s)'])

        # Position in Orbital Plane:
        cosAr = cos(ArgLat)
        sinAr = sin(ArgLat)
        Xk = Radius * cosAr
        Yk = Radius * sinAr

        # WGS-84 Position coordinates:
        cosAs = cos(AscNode)
        sinAs = sin(AscNode)
        cosi = cos(ik)
        sini = sin(ik)
        self.x = Xk * cosAs - Yk * sinAs * cosi
        self.y = Xk * sinAs + Yk * cosAs * cosi
        self.z = Yk * sini

        # Inertial Velocity in WGS-84 coordinates:
        if fabs(1 - e2) < constants.sat_epsilon:
            derpar = 0
        else:
            derpar = sqrt(constants.mu_earth / SemiAxis / (1.- e2))

        rdot = derpar * e1 * sin(trueAnom)
        rLatdot = derpar * (1 + e1 * cos(trueAnom))
        Vxk = rdot * cosAr - rLatdot * sinAr
        Vyk = rdot * sinAr + rLatdot * cosAr
        self.vx = Vxk * cosAs - Vyk * sinAs * cosi
        self.vy = Vxk * sinAs + Vyk * cosAs * cosi
        self.vz = Vyk * sini

        return True

    def _get_sat_range(self, dUserPos, dSVPos):
        '''
        This method computes the user-satellite range, and the unit vector
        from the user to the satellite.
        '''
        auxX = dUserPos[0] - dSVPos[0]
        auxY = dUserPos[1] - dSVPos[1]
        auxZ = dUserPos[2] - dSVPos[2]
        Range = sqrt(auxX**2 + auxY**2 + auxZ**2)

        AproxTransit = Range / constants.speed_of_light
        omegaApT = constants.omega_earth * AproxTransit

        Xga = -auxX + omegaApT * dUserPos[1]
        Yga = -auxY - omegaApT * dUserPos[0]
        Zga = -auxZ

        Range = sqrt(Xga**2 + Yga**2 + Zga**2)
        # Generate unit vector:
        unit = [1, 0, 0]
        if Range > 0:
            unit[0] = Xga/Range
            unit[1] = Yga/Range
            unit[2] = Zga/Range

        return Range, unit

    def is_sat_visible(self, gps_time, coordinates, wgs84=True, rad=True, \
        elevation_threshold=constants.visible_elevation):
        '''
        This method checks if the satellite is visible at a certain gps time
        from a given set of coordinates.
        USAGE: coordinates must be a list of wgs84 coordinates (in radians or
            in degrees) or a list of ECEF coordinates, as long as optional
            parameters 'wgs84' and 'rad' are set correctly. Eg:
            coordinates = [latitude longitude ellipsoidal_height]
            coordinates = [x, y, z]

            Elevation threshold must be given in degrees, and it's value is 5º
            as default.

        Notes:
            (1) This function assumes the WGS84 model.
            (2) Latitude is customary geodetic (not geocentric).
            (3) Tested but no warranty; use at your own risk.
            (4) A satellite is declared as visible if its elevation is over
            the given elevation threshold.
        '''
        visible = False
        El = self.get_elevation(gps_time, coordinates, wgs84=wgs84, rad=rad)
        if El > elevation_threshold:
            visible = True

        return visible, El

    def _locate_satellite(self, gps_time, coordinates, wgs84=True, rad=True):
        '''
        This method does many calculations which are necessary for elevation
        and azimuth calculations for a certain gps time and from a given set
        of coordinates.
        USAGE: coordinates must be a list of wgs84 coordinates (in radians or
            in degrees) or a list of ECEF coordinates, as long as optional
            parameters 'wgs84' and 'rad' are set correctly. Eg:
            coordinates = [latitude longitude ellipsoidal_height]
            coordinates = [x, y, z]

        Notes:
            (1) This function assumes the WGS84 model.
            (2) Latitude is customary geodetic (not geocentric).
            (3) Tested but no warranty; use at your own risk.
        '''
        U1, Nor, aux = None

        # First of all, check if calculations have already been done for given gps time
        if gps_time.get_gps_time() in self.done_calculations:
            U1 = self.done_calculations[gps_time.get_gps_time()][0]
            Nor = self.done_calculations[gps_time.get_gps_time()][1]
            aux = self.done_calculations[gps_time.get_gps_time()][2]
        else:
            try:
                # First get satellite position (if satellite is available) for a given gps time
                satPos = self.get_position(gps_time)

                # User coordinates must be in ECEF, so it msut be checked and converted if necessary
                user_coordinates = []
                lat, lon = None
                if wgs84:
                    if not rad:
                        lat = radians(coordinates[0])
                        lon = radians(coordinates[1])
                    ellipsoidal_height = coordinates[2]
                    x, y, z = geodesy.lla_to_ecef(lat, lon, ellipsoidal_height, rad=True)
                    user_coordinates = [x, y, z]
                else:
                    user_coordinates = coordinates
                    # But latitude and longitude are as well needed for calculations, so:
                    x = coordinates[0]
                    y = coordinates[1]
                    z = coordinates[2]
                    lat, lon, _ = geodesy.ecef_to_lla(x, y, z, rad=True)

                _, U1 = self._get_sat_range(user_coordinates, satPos)

                coslat = cos(lat)
                sinlat = sin(lat)
                coslon = cos(lon)
                sinlon = sin(lon)
                Nor = [None] * 3
                Nor[0] = coslat * coslon
                Nor[1] = coslat * sinlon
                Nor[2] = sinlat
                aux = Nor[0]*U1[0] + Nor[1]*U1[1] + Nor[2]*U1[2]

                # If azimuth and elevation are both requested, the calculations are repeated,
                # so this data can be stored for given gps_time
                self.done_calculations[gps_time.get_gps_time()] = [U1, Nor, aux]
            except Exception as e:
                print e

        return U1, Nor, aux

    def get_elevation(self, gps_time, coordinates, wgs84=True, rad=True):
        '''
        This method calculates satellite elevation at a certain gps time
        from a given set of coordinates.
        USAGE: coordinates must be a list of wgs84 coordinates (in radians or
            in degrees) or a list of ECEF coordinates, as long as optional
            parameters 'wgs84' and 'rad' are set correctly. Eg:
            coordinates = [latitude longitude ellipsoidal_height]
            coordinates = [x, y, z]

        Notes:
            (1) This function assumes the WGS84 model.
            (2) Latitude is customary geodetic (not geocentric).
            (3) Tested but no warranty; use at your own risk.
        '''
        El = None
        U1, Nor, aux = self._locate_satellite(self, gps_time, coordinates, wgs84=wgs84, rad=rad)

        El = pi/2 - acos(aux)
        if not rad:
            El = degrees(El)
        return El

    def get_azimuth(self, gps_time, coordinates, wgs84=True, rad=True):
        '''
        This method calculates satellite azimuth at a certain gps time
        from a given set of coordinates.
        USAGE: coordinates must be a list of wgs84 coordinates (in radians or
            in degrees) or a list of ECEF coordinates, as long as optional
            parameters 'wgs84' and 'rad' are set correctly. Eg:
            coordinates = [latitude longitude ellipsoidal_height]
            coordinates = [x, y, z]

        Notes:
            (1) This function assumes the WGS84 model.
            (2) Latitude is customary geodetic (not geocentric).
            (3) Tested but no warranty; use at your own risk.
        '''
        U1, Nor, aux = self._locate_satellite(self, gps_time, coordinates, wgs84=wgs84, rad=rad)

        StoN = [None] * 3
        StoN[0] = -self.coslon * self.sinlat
        StoN[1] = -self.sinlon * self.sinlat
        StoN[2] = self.coslat

        WtoE = [None] * 3
        WtoE[0] = -self.sinlon
        WtoE[1] = self.coslon
        WtoE[2] = 0

        Hor = [None] * 3
        Hor[0] = U1[0] - aux*Nor[0]
        Hor[1] = U1[1] - aux*Nor[1]
        Hor[2] = U1[2] - aux*Nor[2]

        azimuth = atan2(WtoE[0]*Hor[0]+WtoE[1]*Hor[1]+WtoE[2]*Hor[2], \
         StoN[0]*Hor[0]+StoN[1]*Hor[1]+StoN[2]*Hor[2])
        if not rad:
            azimuth = degrees(azimuth)
        return azimuth
