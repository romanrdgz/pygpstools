#!/usr/bin/env python

from pygpstools.gpstime import GPSTime

gps_times = []
gps_times.append(GPSTime(wn=1751, tow=314880))
gps_times.append(GPSTime(gps_time=1059319680))
gps_times.append(GPSTime(utc=datetime(2013, 7, 31, 15, 28, 0)))
gps_times.append(GPSTime(localtime=datetime(2013, 7, 31, 17, 28, 0)))
gps_times.append(GPSTime())
gps_times.append(GPSTime(gps_time=1059319680) - 80)
gps_times.append(GPSTime(gps_time=1059319680) - GPSTime(gps_time=80))
gps_times.append(GPSTime(gps_time=1059319680) + 20)
gps_times.append(GPSTime(gps_time=1059319680) + GPSTime(gps_time=20))
gps_times.append(20 + GPSTime(gps_time=1059319680))
gps_times.append(GPSTime(gps_time=20) + GPSTime(gps_time=1059319680))
gps_times.append(1059319680 - GPSTime(gps_time=80))
gps_times.append(GPSTime(gps_time=1059319680) - GPSTime(gps_time=80))

for gtime in gps_times:
    print 'GPS time: ' + str(gtime) + ' ---> ' + str(gtime.to_utc()) + \
          ' UTC (local time ' + str(gtime.to_localtime()) + ')'

aux_gps_time = gps_times
if gps_times[0] == aux_gps_time:
	print 'Equal comparison OK'
else:
	print 'Equal comparison FAILED'

aux_gps_time += 120
if gps_times[0] != aux_gps_time:
	print 'Diff comparison OK'
else:
	print 'Diff comparison FAILED'

if (gps_times[0] <= aux_gps_time) 
	and (gps_times[0] < aux_gps_time)
	and (gps_times[0] <= aux_gps_time.gps_time)
	and (gps_times[0] < aux_gps_time.gps_time)
	and (gps_times[0].gps_time <= aux_gps_time)
	and (gps_times[0].gps_time < aux_gps_time):
	print 'LT and LE comparisons OK'
else:
	print 'LT and LE comparisons FAILED'

aux_gps_time -= 240
if (gps_times[0] >= aux_gps_time) 
    and (gps_times[0] > aux_gps_time)
    and (gps_times[0] >= aux_gps_time.gps_time)
    and (gps_times[0] > aux_gps_time.gps_time)
    and (gps_times[0].gps_time >= aux_gps_time)
    and (gps_times[0].gps_time > aux_gps_time):
	print 'GT and GE comparisons OK'
else:
	print 'GT and GE comparisons FAILED'